---
name: "Caleb Connolly"
date: 2020-09-22T15:39:09+01:00
# Delete the following if unused
twitter: "@kalubeee"
github: "calebccff"
gitlab: "kalube"
website: "connolly.tech"
---

Hi I'm Caleb, I'm 19 and currently taking Computer Science at Swansea University, I'm also the current president of SUCS.

<!-- more -->

I enjoy working on [Linux for mobile phones](https://wiki.postmarketos.org/wiki/OnePlus_6_(oneplus-enchilada)) and building software that can benefit everyone, although sometimes the motivation is lacking. he/they