---
name: "{{ replace .Name "-" " " | title }}"
# Delete the following if unused
twitter: "@yourhandle"
github: "github-user"
gitlab: "gitlab-user"
website: "site-url"
tags:
  - untagged
---
