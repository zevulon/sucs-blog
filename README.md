# A SUCS Blog

This blog is designed to be a flat structured site where anyone can write articles on pretty much anything, with a focus on tech and FOSS.
You can find the theme used [here](https://projects.sucs.org/kalube/sucs-hugo-theme)

## How does this work?

The site is written in Hugo, a static site generator, it's designed to be served without needing a webserver/backend

## Status

You can check out the latest testing version of the blog at https://sucs.org/~kalube/blogtest/

CI is currently broken due to some SSH isues, the site will also become available at https://blog.sucs.org once it's ready.

## Feature list

- [X] View posts and tags
- [ ] View authors / have a bio for authors
- [ ] Show latest posts, most popular tags and about section on home page
- [ ] Symposiums - show posts in reply to other posts, current theme etc